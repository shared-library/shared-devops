# Shared Library

Crie o ambiente com Jenkins e Gitlab em container local para testar e estudar o uso da Shared Library no Jenkins.

Para isso digite o comanado baixo.

```bash
docker-compose start
```

## Configuração do Gitlab

1) Abra o GitLab `http://localhost`
2) Configure um usuário admin e um usuário (opcional)
3) Suba o código shared-devops para o Gitlab local

```bash
git remote add local http://localhost/<usuario>/shared
```

3) Suba também o código do app-sample apra o Gitlab local

```bash
git remote add local http://localhost/<usuario>/app-sample
```

## Configuração do Jenkins

1) Pegue a senha para prosseguir na configuração do Jenkins

```bash
docker exec <container_name> cat /var/jenkins_home/secrets/initialAdminPassword
```

2) Acesse o Jenkins `http://localhost:8080`
3) Configure um usuário e senha de administração do Jenkins
4) Instale os plugins recomendadados durante o processo de configuração e instale também o plugin [Pipeline Utility Steps](https://plugins.jenkins.io/pipeline-utility-steps/)

## Configuração Jenkins Shared Library

Após o processo de configuração do Jenkins iremos configurar a Shared Library.

1. Clique em `Manage Jenkins` > `Configure System`
2. Vá na seção `Global Pipeline Libraries`
3. Clique em `Add` e preencha as informações conforme informado abaixo:

Name: **shared**
Default version: **main**

Retrieval method: **Modern SCM**

Source Code Management: **Git**

Project Repository: **http://gitlab/<usuario>/shared.git**

Credentials: **crie uma credencial com o seu usuário e senha do Gitlab local**

## Criando a Pipeline

Vamos agora criar a pipeline da aplicação app-sample.

1. Acesse o Jenkins `http://localhost:8080`
2. Clique em `New Item`, coloque o nome do pipeline como `myapp` e escolha a opção `Pipeline` e clique em `ok`
3. Com a pipeline criada clique em `Configure` e vá na seção `Pipeline` e preencha as informações conforme abaixo:

Definition: **Pipeline script from SCM**

SCM: **Git**

Repository URL: **http://gitlab/<usuario>/app-sample.git**

Credentials: **use a mesma credencial criada anteriormente**

Branch Speficier: **main**

Script Path: **Jenkinsfile**

4. Vá na seção `General` e marque a opção: `This project is parameterized`
   1. Clique em `Add Parameter`
   2. Escolha a opção `String Parameter` e preencha as informações conforme abaixo:

Name: **TECHNOLOGY**

Default Value: **NODEJS**

5. Clique em `Save`

## Executando a Pipeline com Shared Library

1. No menu clique em `Build with Parameters`
2. Clique em `Build`

## Job DSL

É necessário instalar o plugin [Job DSL](https://plugins.jenkins.io/job-dsl/) no Jenkins.

Também é necessário desativar uma funcionalidade de segurança para que o script do Job DSL possa rodar sem a necessidade de aprovação.
Para fazer essa configuaração siga os passos abaixos:

1) Acesse o `Manage Jenkins` 
2) Vá em `Configure Global Security`
3) Na seção de CSRF desmarque a opção `Enable script security for Job DSL scripts`

## Chamada do Job via API

```bash
curl -X GET http://localhost:8080/crumbIssuer/api/json -u admin:admin
```

```bash
curl -X POST \
-H Jenkins-Crumb:<cumbIssuer> \
http://localhost:8080/job/job-devops/buildWithParameters?token=<token-job> \
--data 'PLATFORM=Passei a descricao do Job via API' \
-u admin:<token-usuário>
```

## RoadMap

Adicionar o [JCasC](https://github.com/jenkinsci/configuration-as-code-plugin/blob/master/README.md) para o Jenkins já subir com todas as configurações necessárias.

## Referências

https://www.jvt.me/posts/2021/02/23/getting-started-jobdsl-standardised/