package br.com.devops

enum TechnologyEnum {
    NODEJS("NodeJSPipeline")

    String value
    TechnologyEnum(String value) {
        this.value = value
    }

    String getValue() {
        return value
    }
}