package br.com.devops

interface Pipeline {
    void build()
    void unitTest()
    void scanSonar()
    void scanFortify()
}
