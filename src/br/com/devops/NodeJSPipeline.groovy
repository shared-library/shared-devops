package br.com.devops

class NodeJSPipeline implements Pipeline, Serializable {
    def steps
    Map pipeConfig

    NodeJSPipeline(def steps, Map pipeConfig) {
        this.steps = steps
        this.pipeConfig = pipeConfig
    }

    @Override
    void build() {
        steps.echo('script build NODEJS')
        if (pipeConfig.build) {
            steps.echo(pipeConfig.testCommand)
        } else {
            steps.echo('Default command')
        }
    }

    @Override
    void unitTest() {
        steps.echo('script unitTest NODEJS')
        if (pipeConfig.testCommand) {
            steps.echo(pipeConfig.testCommand)
        } else {
            steps.echo('Default command')
        }
    }

    @Override
    void scanSonar() {
        steps.echo('script scanSonar NODEJS')
        if (pipeConfig.scanSonar) {
            steps.echo(pipeConfig.testCommand)
        } else {
            steps.echo('Default command')
        }
    }

    @Override
    void scanFortify() {
        if (pipeConfig.fortify == true) {
            steps.echo('script scanFortify NODEJS')
            if (pipeConfig.scanFortify) {
                steps.echo(pipeConfig.testCommand)
            } else {
                steps.echo('Default command')
            }
        } else {
            steps.echo('Fortify Scan is deactivated')
        }
    }
}
