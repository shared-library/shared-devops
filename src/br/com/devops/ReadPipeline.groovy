package br.com.devops

class ReadPipeline {
    def steps

    ReadPipeline(steps) {
        this.steps = steps
    }

    Map getConfig() {
        Map pipelineConfig = steps.readYaml(file: 'pipeline.yml')
        return pipelineConfig
    }
}