package br.com.devops

class ExecutePipeline implements Serializable {
    def clazz
    def steps

    ExecutePipeline(steps) {
        this.steps = steps
    }

    void start() {
        steps.node {
            steps.stage('Checkout') {
                steps.checkout steps.scm
            }
            steps.stage('Setup') {
                String tech = steps.params.TECHNOLOGY
                Map pipeConfig = new ReadPipeline(steps).getConfig()
                for (item in TechnologyEnum.values()) {
                    if (item.name().equalsIgnoreCase(tech)) {
                        steps.echo('Technology selected: ' + tech)
                        clazz = this.class.classLoader.loadClass('br.com.devops.' + item.value).newInstance(steps, pipeConfig)
                    }
                }
                if (!clazz) {
                    steps.error('Technology not found')
                }
            }
            steps.stage('Build') {
                clazz.build()
            }

            steps.stage('Unit Test') {
                clazz.unitTest()
            }

            steps.stage('Scan Sonar') {
                clazz.scanSonar()
            }

            steps.stage('Scan Fortify') {
                clazz.scanFortify()
            }
        }
    }
}
