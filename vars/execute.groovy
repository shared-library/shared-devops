#!/usr/bin/env groovy
import br.com.devops.ExecutePipeline

def call() {
    new ExecutePipeline(this).start()
}